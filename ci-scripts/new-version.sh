#!/bin/bash

git clone --single-branch --branch "$CI_DEFAULT_BRANCH" https://gitlab-ci-token:"$ACCESS_TOKEN"@gitlab.com/"$CI_PROJECT_PATH".git
cd "$CI_PROJECT_NAME" || exit 1

BUMP_TYPE=''
for main_tag in major minor path
do
  if [[ $BUMP_TYPE != '' ]]; then
      break
  fi

  for tag in $(git tag -l "$main_tag-*")
  do
    sha=$(git rev-parse $tag)
    echo -e "\e[34mtag: $tag, sha: $sha\e[0m";
    if [[ $(git branch --contains "$sha" | grep "$CI_DEFAULT_BRANCH") ]]; then
        BUMP_TYPE="$main_tag"
        echo -e "\e[34mVersion bump type is: $BUMP_TYPE\e[1;34m"
        break
    fi
  done
done
CURRENT_TAG=$(git tag -l | sort -rV | grep '^v[0-9]*\.[0-9]*\.[0-9]$' | head -n 1)
echo -e "\e[34mCurrent tag is: $CURRENT_TAG"
if [[ $BUMP_TYPE == 'major' ]]; then
  echo -e "\e[92mMajor - incombatible to prevoius version changes\e[0m"
  [[ "$CURRENT_TAG" =~ v([0-9])*\. ]] && \
    NEW_TAG="v$((BASH_REMATCH[1] + 1 )).0.0"

elif [[ $BUMP_TYPE == 'minor' ]]; then
  echo -e "\e[92mMinor - new features with backward combatibility\e[0m"
  [[ "$CURRENT_TAG" =~ v([0-9])*\.([0-9])*\. ]] && \
    NEW_TAG="v${BASH_REMATCH[1]}.$((BASH_REMATCH[2] + 1 )).0"

elif [[ $BUMP_TYPE == 'path' ]]; then
  echo -e "\e[92mPath - bug fixes\e[0m"
  [[ "$CURRENT_TAG" =~ v([0-9])*\.([0-9])*\.([0-9])* ]] && \
    NEW_TAG="v${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.$((BASH_REMATCH[3] + 1 ))"

else
  NEW_TAG="v1.0.0"
fi

echo -e "\e[1;92mNew tag is: $NEW_TAG\e[0m"

git tag -am "Create version $NEW_TAG" $NEW_TAG || exit 1

git push origin $NEW_TAG

for tag in $(git tag -l)
do
  if [[ $(git branch --contains $sha | grep "$CI_DEFAULT_BRANCH") ]] && \
   [[ ! "$tag" =~ ^v([[:digit:]])*\.([[:digit:]]*)\.([[:digit:]])$ ]]; then
      git tag -d "$tag"
      git push --delete origin "$tag"
      echo -e "\e[92mDeleted tag: $tag\e[0m"
  fi
done
