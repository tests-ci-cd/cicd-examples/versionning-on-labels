#!/bin/bash

git clone --single-branch --branch "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" https://gitlab-ci-token:"$ACCESS_TOKEN"@gitlab.com/"$CI_PROJECT_PATH".git
cd "$CI_PROJECT_NAME" || exit 1
for tag in major minor path
do
  if [[ $(git tag -l "$tag-$CI_MERGE_REQUEST_IID") ]]; then
      git tag -d "$tag-$CI_MERGE_REQUEST_IID"
      git push --delete origin "$tag-$CI_MERGE_REQUEST_IID"
  fi
done

echo -e "\e[92mOld tags cleaned\e[0m"

if [[ ! $CI_MERGE_REQUEST_LABELS =~ (,|^)(major|minor|path)(,|$) ]]; then
  echo -e "\e[91;1mVersion mark labels is missing!"
  echo -e "\e[91;1mNeed one of them: major | minor | path\e[0m"
  exit 1
fi


if [[ $CI_MERGE_REQUEST_LABELS =~ (,|^)major(,|$) ]]; then
  TAG=major
elif [[ $CI_MERGE_REQUEST_LABELS =~ (,|^)minor(,|$) ]]; then
  TAG=minor
elif [[ $CI_MERGE_REQUEST_LABELS =~ (,|^)path(,|$) ]]; then
  TAG=path
fi

echo -e "\e[34mMR increase project on \e[1;34m$TAG\e[34m version\e[0m"

git tag -am "Merge Request: !$CI_MERGE_REQUEST_IID" "$TAG-$CI_MERGE_REQUEST_IID"
git push origin "$TAG-$CI_MERGE_REQUEST_IID" HEAD:refs/heads/"$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
true
