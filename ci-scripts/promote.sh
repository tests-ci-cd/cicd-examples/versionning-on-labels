#!/bin/bash

git clone --single-branch --branch "$CI_DEFAULT_BRANCH" https://gitlab-ci-token:"$ACCESS_TOKEN"@gitlab.com/"$CI_PROJECT_PATH".git
cd "$CI_PROJECT_NAME" || exit 1

echo -e "\e[92mPush to external repository\e[0m"

git push --follow-tags https://gitlab-ci-token:"$ACCESS_TOKEN"@gitlab.com/tests-ci-cd/cicd-examples/promoted.git
